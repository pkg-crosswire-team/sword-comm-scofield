\id 3JN
\toc1
\toc2
\toc3
\c 1
\s Book Introduction - 3 John
\p @@reference osisRef="3jo 1"µµRead first chapter of 3 John@@/referenceµµ
\p \bd WRITER\bd*: The Apostle John.
\p \bd DATE\bd*: Probably about A.D. 90.
\p \bd THEME\bd*: The aged Apostle had written to a church which allowed one Diotrephes to exercise an authority common enough in later ages, but wholly new in the primitive churches. Diotrephes had rejected the apostolic letters and authority. It appears also that he had refused the ministry of the visiting brethren (@@reference osisRef="3jo 1:10"µµ3 John 1:10@@/referenceµµ), and cast out those that had received them. Historically, this letter marks the beginning of that clerical and priestly assumption over the churches in which the primitive church order disappeared. This Epistle reveals, as well, the believer's resource in such a day. No longer writing as an apostle, but as an elder, John addresses this letter, not to the church as such, but to a faithful man in the church for the comfort and encouragement of those who were standing fast in the primitive simplicity. Second John conditions the personal walk of the Christian in a day of apostasy; Third John the personal responsibility in such a day of the believer as a member of the local church. The key-phrase is "the truth" (see 2 John, Introduction).
\p There are three divisions:
\li Personal greetings, vs. 1-4
\li Instructions concerning ministering brethren, vs. 5-8
\li The apostate leader and the good Demetrius, vs. 9-14 </ol>
