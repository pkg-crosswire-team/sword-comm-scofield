\id DEU
\toc1
\toc2
\toc3
\c 1
\s Book Introduction - Deuteronomy
\p @@reference osisRef="de 1"µµRead first chapter of Deuteronomy@@/referenceµµ
\p DEUTERONOMY consists of the parting counsels of Moses delivered to Israel in view of the impending entrance upon their covenanted possession. It contains a summary of the wilderness wanderings of Israel, which is important as unfolding the moral judgement of God upon those events; repeats the Decalogue to a generation which had grown up in the wilderness; gives needed instruction as the conduct of Israel in the land, and contains the Palestinian Covenant (@@reference osisRef="de 30:1-9"µµDeuteronomy 30:1-9@@/referenceµµ). The book breathes the sternness of the Law. Key- words, "Thou shalt"; key-verses, @@reference osisRef="de 11:26-28"µµDeuteronomy 11:26-28@@/referenceµµ.
\p It is important to note that, while the land of promise was unconditionally given Abraham and to his seed in the Abrahamic Covenant (@@reference osisRef="ge 13:15"µµGenesis 13:15@@/referenceµµ; @@reference osisRef="ge 15:7"µµ 15:7@@/referenceµµ), it was under the conditional Palestinian Covenant (@@reference osisRef="de 28:1-de 30:9"µµDeuteronomy 28:1-30:9@@/referenceµµ) that Israel entered the land under Joshua. Utterly violating the conditions of that covenant, the nation was first disrupted (1 Kings 12) and then cast out of the land (@@reference osisRef="2ki 17:1-18"µµ2 Kings 17:1-18@@/referenceµµ; @@reference osisRef="2ki 24:1-2ki 25:11"µµ 24:1-25:11@@/referenceµµ). But the same covenant unconditionally promises a national restoration of Israel which is yet to be fulfilled \it (See Scofield "@@reference osisRef="Scofield:De 15:18"µµDe 15:18@@/referenceµµ")\it*.
\p DEUTERONOMY is in seven divisions:
\li Summary of the history of Israel in the wilderness, 1:1-3:29
\li A restatement of the Law, with warnings and exhortations, 4:1 -11:32,
\li Instructions, Warnings, and Predictions, 12:1-27:26,
\li The great closing prophecies summarizing the history of Israel to the second coming of Christ, and containing the Palestinian Covenant, 28:1-30:20,
\li Last counsels to Priests, Levites, and to Joshua, 31,
\li The Song of Moses and his parting blessings, 32,33,
\li The Death of Moses, 34.
\p The time covered by this retrospect is approximately forty years.
\v 2 \bd eleven days\bd*
\p Prolonged by one act of unbelief to forty years. \it (See Scofield "@@reference osisRef="Scofield:Nu 14:23"µµNumbers 14:23@@/referenceµµ")\it*.
\v 3 \bd eleventh\bd*
\p i.e. February.
\c 2
\p
\v 1
\v 9 \bd given Ar\bd*
\p A region east of the Dead Sea.
\c 3
\p
\v 1
\v 17 \bd Ashdoth-pisgah\bd*
\p i.e. the springs of Pisgah, or the hill.
\c 4
\p
\v 1
\v 10 \bd fear\bd*
\p \it (See Scofield "@@reference osisRef="Scofield:Ps 19:9"µµPsalms 19:9@@/referenceµµ")\it*.
\v 15 \bd saw no manner\bd*
\p Cf. \it (See Scofield "@@reference osisRef="Scofield:Joh 1:18"µµJohn 1:18@@/referenceµµ")\it*.
\c 5
\p
\v 1
\v 29 \bd fear\bd*
\p \it (See Scofield "@@reference osisRef="Scofield:Ps 19:9"µµPsalms 19:9@@/referenceµµ")\it*.
\c 6
\p
\v 1
\v 13 \bd fear\bd*
\p \it (See Scofield "@@reference osisRef="Scofield:Ps 19:9"µµPsalms 19:9@@/referenceµµ")\it*.
\c 7
\p
\v 1
\v 8 \bd redeemed\bd*
\p \it (See Scofield "@@reference osisRef="Scofield:Ex 14:30"µµExodus 14:30@@/referenceµµ")\it*.
\c 8
\p
\v 1
\v 6 \bd fear\bd*
\p \it (See Scofield "@@reference osisRef="Scofield:Ps 19:9"µµPsalms 19:9@@/referenceµµ")\it*.
\c 9
\p
\v 1
\v 10 \bd And the Lord\bd*
\p \it (See Scofield "@@reference osisRef="Scofield:Ex 20:4"µµExodus 20:4@@/referenceµµ")\it*.
\v 26 \bd redeemed\bd*
\p \it (See Scofield "@@reference osisRef="Scofield:Ex 14:30"µµExodus 14:30@@/referenceµµ")\it*.
\c 10
\p
\v 1
\v 3 \bd shittim\bd*
\p i.e. acacia.
\v 12 \bd fear\bd*
\p \it (See Scofield "@@reference osisRef="Scofield:Ps 19:9"µµPsalms 19:9@@/referenceµµ")\it*.
\v 20 \bd fear\bd*
\p \it (See Scofield "@@reference osisRef="Scofield:Ps 19:9"µµPsalms 19:9@@/referenceµµ")\it*.
\c 13
\p
\v 1
\v 4 \bd fear\bd*
\p \it (See Scofield "@@reference osisRef="Scofield:Ps 19:9"µµPsalms 19:9@@/referenceµµ")\it*.
\v 5 \bd redeemed\bd*
\p \it (See Scofield "@@reference osisRef="Scofield:Ex 14:30"µµExodus 14:30@@/referenceµµ")\it*.
\c 14
\p
\v 1
\v 7 \bd hare\bd*
\p \it (See Scofield "@@reference osisRef="Scofield:Le 11:6"µµLeviticus 11:6@@/referenceµµ")\it*.
\v 23 \bd fear\bd*
\p \it (See Scofield "@@reference osisRef="Scofield:Ps 19:9"µµPsalms 19:9@@/referenceµµ")\it*.
\c 15
\p
\v 15 \bd redeemed\bd*
\p See note, \it (See Scofield "@@reference osisRef="Scofield:Ex 14:30"µµExodus 14:30@@/referenceµµ")\it*.
\p
\c 16
\p
\v 1 \bd keep the passover\bd*
\p Cf. the order of the feasts in Lev. 23. Here the Passover and Tabernacles are given especial emphasis as marking the beginning and the consummation of God's ways with Israel; the former speaking of redemption, the foundation of all: the latter, or re-gathered Israel blessed in the kingdom. Between, in @@reference osisRef="de 16:9-12"µµDeuteronomy 16:9-12@@/referenceµµ comes the Feast of Weeks--the joy of a redeemed people, anticipating a greater blessing yet to come. It is, morally, @@reference osisRef="ro 5:1 ro 5:2"µµRomans 5:1,2@@/referenceµµ.
\p \bd Abib\bd* First month i.e. April.
\v 21 \bd grove\bd*
\p The groves (Heb. Asherim) so often mentioned in the OT were devoted to the worship of Ashtereth, the Babylonian goddess Ishtar, the Aphrodite of the Greeks, the Roman Venus. CF.
\p \it (See Scofield "@@reference osisRef="Scofield:Jud 2:13"µµJudges 2:13@@/referenceµµ")\it*.
\c 17
\p
\v 19 \bd fear\bd*
\p \it (See Scofield "@@reference osisRef="Scofield:Ps 19:9"µµPsalms 19:9@@/referenceµµ")\it*.
\c 19
\p
\v 6 \bd avenger\bd*
\p Heb. "goel," Redemp. (Kinsman type). \it (See Scofield "@@reference osisRef="Scofield:Isa 59:20"µµIsaiah 59:20@@/referenceµµ")\it*.
\c 21
\p
\v 8 \bd Israel\bd*
\p \it (See Scofield "@@reference osisRef="Scofield:Ex 14:30"µµExodus 14:30@@/referenceµµ")\it*.
\c 24
\p
\v 18 \bd redeemed\bd*
\p \it (See Scofield "@@reference osisRef="Scofield:Ex 14:30"µµExodus 14:30@@/referenceµµ")\it*.
\p
\c 28
\p
\v 1 \bd if thou shalt hearken\bd*
\p Chapters 28.-29. are, properly, an integral part of the Palestinian covenant, @@reference osisRef="de 30:1-9"µµDeuteronomy 30:1-9@@/referenceµµ. \it (See Scofield "@@reference osisRef="Scofield:De 30:3"µµDeuteronomy 30:3@@/referenceµµ")\it*.
\v 52 \bd trustedst\bd*
\p \it (See Scofield "@@reference osisRef="Scofield:Ps 2:12"µµPsalms 2:12@@/referenceµµ")\it*.
\v 58 \bd fear\bd*
\p \it (See Scofield "@@reference osisRef="Scofield:Ps 19:9"µµPsalms 19:9@@/referenceµµ")\it*.
\c 30
\p
\v 3 \bd turn thy captivity\bd*
\p The Palestinian Covenant gives the conditions under which Israel entered the land of promise. It is important to see that the nation has never as yet taken the land under the unconditional Abrahamic Covenant, nor has it ever possessed the whole land (cf. @@reference osisRef="ge 15:18"µµGenesis 15:18@@/referenceµµ; @@reference osisRef="nu 34:1-12"µµNumbers 34:1-12@@/referenceµµ. The Palestinian Covenant is in seven parts.
\p
\li (1) Dispersion for disobedience, @@reference osisRef="de 30:1"µµDeuteronomy 30:1@@/referenceµµ; @@reference osisRef="de 28:63-68"µµ 28:63-68@@/referenceµµ \it (See Scofield "@@reference osisRef="Scofield:Ge 15:18"µµGenesis 15:18@@/referenceµµ")\it*
\li (2) The future repentance of Israel while in the dispersion, @@reference osisRef="de 30:2"µµDeuteronomy 30:2@@/referenceµµ.
\li (3) The return of the Lord, @@reference osisRef="de 30:3"µµDeuteronomy 30:3@@/referenceµµ; @@reference osisRef="am 9:9-14"µµAmos 9:9-14@@/referenceµµ; @@reference osisRef="ac 15:14-17"µµActs 15:14-17@@/referenceµµ.
\li (4) Restoration to the land, @@reference osisRef="de 30:5"µµDeuteronomy 30:5@@/referenceµµ; @@reference osisRef="isa 11:11 isa 11:12"µµIsaiah 11:11,12@@/referenceµµ; @@reference osisRef="jer 23:3-8"µµJeremiah 23:3-8@@/referenceµµ; @@reference osisRef="eze 37:21-25"µµEzekiel 37:21-25@@/referenceµµ
\li (5) National conversion, @@reference osisRef="de 30:6"µµDeuteronomy 30:6@@/referenceµµ; @@reference osisRef="ro 11:26 ro 11:27"µµRomans 11:26,27@@/referenceµµ; @@reference osisRef="ho 2:14-16"µµHosea 2:14-16@@/referenceµµ
\li (6) The judgment of Israel's oppressors, @@reference osisRef="de 30:7"µµDeuteronomy 30:7@@/referenceµµ; @@reference osisRef="isa 14:1 isa 14:2"µµIsaiah 14:1,2@@/referenceµµ; @@reference osisRef="joe 3:1-8"µµJoel 3:1-8@@/referenceµµ; @@reference osisRef="mt 25:31-46"µµMatthew 25:31-46@@/referenceµµ
\li (7) National prosperity, @@reference osisRef="de 30:9"µµDeuteronomy 30:9@@/referenceµµ; @@reference osisRef="am 9:11-14"µµAmos 9:11-14@@/referenceµµ
\p For Another Point of View: See Topic 301242
\p See, for the other seven covenants:
\p EDENIC \it (See Scofield "@@reference osisRef="Scofield:Ge 1:28"µµGenesis 1:28@@/referenceµµ")\it* ADAMIC See Scofield "@@reference osisRef="Scofield:ge 3:15"µµGenesis 3:15@@/referenceµµ" ABRAHAMIC See Scofield "@@reference osisRef="Scofield:ge 15:18"µµGenesis 15:18@@/referenceµµ" NOAHIC See Scofield "@@reference osisRef="Scofield:ge 9:1"µµGenesis 9:1@@/referenceµµ" MOSAIC See Scofield "@@reference osisRef="Scofield:ex 19:25"µµExodus 19:25@@/referenceµµ" DAVIDIC See Scofield "@@reference osisRef="Scofield:2sa 7:16"µµ2 Samuel 7:16@@/referenceµµ" NEW See Scofield "@@reference osisRef="Scofield:heb 8:8"µµHebrews 8:8@@/referenceµµ"
\c 31
\p
\v 12 \bd fear\bd*
\p \it (See Scofield "@@reference osisRef="Scofield:Ps 19:9"µµPsalms 19:9@@/referenceµµ")\it*.
\c 32
\p
\v 36 \bd repent\bd*
\p \it (See Scofield "@@reference osisRef="Scofield:Zec 8:14"µµZechariah 8:14@@/referenceµµ")\it*.
\v 37 \bd trusted\bd*
\p \it (See Scofield "@@reference osisRef="Scofield:Ps 2:12"µµPsalms 2:12@@/referenceµµ")\it*.
\c 33
\p
\v 8 \bd Thummim\bd*
\p \it (See Scofield "@@reference osisRef="Scofield:Ex 28:30"µµExodus 28:30@@/referenceµµ")\it*.
