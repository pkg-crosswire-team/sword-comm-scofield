\id 1JN
\toc1
\toc2
\toc3
\c 1
\s Book Introduction - 1 John
\p @@reference osisRef="1jo 1"µµRead first chapter of 1 John@@/referenceµµ
\p \bd WRITER\bd*: The Apostle John, as unbroken tradition affirms, and as internal evidence and comparison with the Gospel of John prove.
\p \bd DATE\bd*: Probably A.D. 90
\p \bd THEME\bd*: First John is a family letter from the Father to His "little children" who are in the world. With the possible exception of the Song, it is the most intimate of the inspired writings. The world is viewed as without. The sin of a believer is treated as a child's offence against his Father, and is dealt with as a family matter (@@reference osisRef="1jo 1:9"µµ1 John 1:9@@/referenceµµ; @@reference osisRef="1jo 2:1"µµ 2:1@@/referenceµµ). The moral government of the universe is not in question. The child's sin as an offence against the law had been met in the Cross, and "Jesus Christ the righteous" is now his "Advocate with the Father." John's Gospel leads across the threshold of the Father's house; his first Epistle makes us at home there. A tender word is used for "children," teknia, "born ones," or "bairns." Paul is occupied with our public position as sons; John with our nearness as born-ones of the Father.
\p First John is in two principal divisions.
\li The family with the Father, 1:1-3:24.
\li The family and the world, 4:1-5:21.
\p There is a secondary analysis in each division of which occurs the phrase, "My little children," as follows:
\li Introductory, the incarnation, 1:1,2.
\li The little children and fellowship1:3-2:14
\li The little children and secular and "religious" world2:15-28.
\li How the little children may know each other, 2:29-3:10
\li How the little children must live together, 3:11-24.
\li Parenthetic: How the little children may know false teachers, 4:1-6.
\li The little children assured and warned, 4:7-5:21.
\p
\v 1 \bd grace\bd* Grace (imparted). Summary (see "Grace,") \it (See Scofield "@@reference osisRef="Scofield:Joh 1:17"µµJohn 1:17@@/referenceµµ")\it* grace is not only dispensationally a method of divine dealing in salvation
\p \it (See Scofield "@@reference osisRef="Scofield:Joh 1:17"µµJohn 1:17@@/referenceµµ")\it* but is also the method of God in the believer's life and service. As saved, he is "not under the law, but under grace" @@reference osisRef="ro 6:14"µµRomans 6:14@@/referenceµµ. Having by grace brought the believer into the highest conceivable position. @@reference osisRef="eph 1:6"µµEphesians 1:6@@/referenceµµ. God ceaselessly works through grace, to impart to, and perfect in him, corresponding graces ; @@reference osisRef="joh 15:4 joh 15:5"µµJohn 15:4,5@@/referenceµµ; @@reference osisRef="ga 5:22 ga 5:23"µµGalatians 5:22,23@@/referenceµµ.
\p Grace, therefore, stands connected with service @@reference osisRef="ro 12:6"µµRomans 12:6@@/referenceµµ; @@reference osisRef="ro 15:15 ro 15:16"µµ 15:15,16@@/referenceµµ; @@reference osisRef="1co 1:3-7"µµ1 Corinthians 1:3-7@@/referenceµµ; @@reference osisRef="1co 3:10"µµ 3:10@@/referenceµµ; @@reference osisRef="1co 15:10"µµ 15:10@@/referenceµµ; @@reference osisRef="2co 12:9 2co 12:10"µµ2 Corinthians 12:9,10@@/referenceµµ; @@reference osisRef="ga 2:9"µµGalatians 2:9@@/referenceµµ; @@reference osisRef="eph 3:7 eph 3:8"µµEphesians 3:7,8@@/referenceµµ; @@reference osisRef="eph 4:7"µµEphesians 4:7@@/referenceµµ; @@reference osisRef="php 1:7"µµPhilippians 1:7@@/referenceµµ; @@reference osisRef="2ti 2:1 2ti 2:2"µµ2 Timothy 2:1,2@@/referenceµµ; @@reference osisRef="1pe 4:10"µµ1 Peter 4:10@@/referenceµµ with Christian growth ; @@reference osisRef="2co 1:12"µµ2 Corinthians 1:12@@/referenceµµ; @@reference osisRef="eph 4:29"µµEphesians 4:29@@/referenceµµ; @@reference osisRef="col 3:16"µµColossians 3:16@@/referenceµµ; @@reference osisRef="col 4:6"µµ 4:6@@/referenceµµ; @@reference osisRef="2th 1:12"µµ2 Thessalonians 1:12@@/referenceµµ; @@reference osisRef="heb 4:16"µµHebrews 4:16@@/referenceµµ; @@reference osisRef="heb 12:28 heb 12:29"µµ 12:28,29@@/referenceµµ; @@reference osisRef="heb 13:9"µµ 13:9@@/referenceµµ; @@reference osisRef="jas 4:6"µµJames 4:6@@/referenceµµ; @@reference osisRef="1pe 1:2"µµ1 Peter 1:2@@/referenceµµ; @@reference osisRef="1pe 3:7"µµ 3:7@@/referenceµµ; @@reference osisRef="1pe 5:5 1pe 5:10"µµ 5:5,10@@/referenceµµ; @@reference osisRef="2pe 3:18"µµ2 Peter 3:18@@/referenceµµ; @@reference osisRef="jude 1:4"µµJude 1:4@@/referenceµµ and with giving ; @@reference osisRef="2co 4:15"µµ2 Corinthians 4:15@@/referenceµµ; @@reference osisRef="2co 8:1 2co 8:6 2co 8:7 2co 8:19"µµ 8:1,6,7,19@@/referenceµµ; @@reference osisRef="2co 9:14"µµ 9:14@@/referenceµµ
\p \bd grace\bd* Grace (imparted). @@reference osisRef="ro 6:1"µµRomans 6:1@@/referenceµµ; @@reference osisRef="2pe 3:18"µµ2 Peter 3:18@@/referenceµµ.
\v 7 \bd light\bd*
\p What it is to "walk in the light" is explained by @@reference osisRef="1jo 1:8-10"µµ1 John 1:8-10@@/referenceµµ. "All things...are made manifest by the light" @@reference osisRef="eph 5:13"µµEphesians 5:13@@/referenceµµ The presence of God brings the consciousness of sin in the nature @@reference osisRef="1jo 1:8"µµ1 John 1:8@@/referenceµµ and sins in the life @@reference osisRef="1jo 1:9 1jo 1:10"µµ1 John 1:9,10@@/referenceµµ. The blood of Christ is the divine provision for both. To walk in the light is to live in fellowship with the Father and the Son. Sin interrupts, but confession restores that fellowship. Immediate confession keeps the fellowship unbroken.
\p \bd light\bd* \it (See Scofield "@@reference osisRef="Scofield:Ex 27:20"µµExodus 27:20@@/referenceµµ")\it*
\c 2
\p
\v 1 \bd advocate\bd*
\p Advocacy is that work of Jesus Christ for sinning saints which He carries on with the Father whereby, because of the eternal efficacy of His own sacrifice, He restores them to fellowship. CF @@reference osisRef="ps 23:3"µµPsalms 23:3@@/referenceµµ. \it (See Scofield "@@reference osisRef="Scofield:Joh 13:10"µµJohn 13:10@@/referenceµµ")\it*.
\p \bd sin\bd* Sin. \it (See Scofield "@@reference osisRef="Scofield:Ro 3:23"µµRomans 3:23@@/referenceµµ")\it*.
\v 2 \bd propitiation\bd* (Greek - \w ἱλασμός|strong="G2434"\w*, that which propitiates). \it (See Scofield "@@reference osisRef="Scofield:Ro 3:25"µµRomans 3:25@@/referenceµµ")\it*.
\p \bd the sins\bd* Omit words "the sins.".
\p \bd world\bd* kosmos = mankind. \it (See Scofield "@@reference osisRef="Scofield:Mt 4:8"µµMatthew 4:8@@/referenceµµ")\it*.
\v 3 \bd commandments\bd*
\p John uses "commandments":
\li (1) in the general sense of the divine will, however revealed, "his word" (@@reference osisRef="1Jn 2:5"µµ1Jn 2:5@@/referenceµµ); and
\li (2) especially of the law of Christ @@reference osisRef="ga 6:2"µµGalatians 6:2@@/referenceµµ; @@reference osisRef="2jo 1:5"µµ2 John 1:5@@/referenceµµ. See, also, @@reference osisRef="joh 15:10-12"µµJohn 15:10-12@@/referenceµµ.
\p
\v 5 \bd perfect\bd*
\p \it (See Scofield "@@reference osisRef="Scofield:Mt 5:48"µµMatthew 5:48@@/referenceµµ")\it*.
\v 12 \bd sins\bd*
\p Sin. \it (See Scofield "@@reference osisRef="Scofield:Ro 3:23"µµRomans 3:23@@/referenceµµ")\it*.
\v 13 \bd little children\bd*
\p The little ones of the family; \it (See Scofield "@@reference osisRef="Scofield:1Jo 2:28"µµ1 John 2:28@@/referenceµµ")\it*.
\v 15 \bd world\bd*
\p kosmos = world-system. @@reference osisRef="1jo 3:13"µµ1 John 3:13@@/referenceµµ; @@reference osisRef="joh 7:7"µµJohn 7:7@@/referenceµµ. \it (See Scofield "@@reference osisRef="Scofield:Re 13:8"µµRevelation 13:8@@/referenceµµ")\it*.
\v 19 \bd went out from us\bd*
\p "Went out from us," that is, doctrinally. Doubtless then, as now, the deniers of the Son @@reference osisRef="1jo 2:22 1jo 2:23"µµ1 John 2:22,23@@/referenceµµ still called themselves Christians. Cf @@reference osisRef="2ti 1:15"µµ2 Timothy 1:15@@/referenceµµ.
\v 28 \bd little children\bd*
\p The general term for all children.
\c 3
\p
\v 1 \bd world\bd*
\p kosmos = mankind. \it (See Scofield "@@reference osisRef="Scofield:Mt 4:8"µµMatthew 4:8@@/referenceµµ")\it*.
\v 4 \bd committeth sin\bd* practiseth sin practiseth also lawlessness; and sin is lawlessness.
\p \bd sin\bd* Sin. \it (See Scofield "@@reference osisRef="Scofield:Ro 3:23"µµRomans 3:23@@/referenceµµ")\it*
\v 7 \bd righteousness\bd*
\p "Righteousness" here, and in the passages having marginal references to this, means the righteous life which is the result of salvation through Christ. The righteous man under law became righteous by doing righteously; under grace he does righteously because he has been made righteous @@reference osisRef="ro 3:22"µµRomans 3:22@@/referenceµµ.
\p \it (See Scofield "@@reference osisRef="Scofield:Ro 10:3"µµRomans 10:3@@/referenceµµ")\it*
\v 10 \bd righteousness\bd*
\p \it (See Scofield "@@reference osisRef="Scofield:1Jo 3:7"µµ1 John 3:7@@/referenceµµ")\it*.
\v 13 \bd world\bd*
\p kosmos = world-system. @@reference osisRef="1jo 4:3-5"µµ1 John 4:3-5@@/referenceµµ; @@reference osisRef="joh 7:7"µµJohn 7:7@@/referenceµµ. \it (See Scofield "@@reference osisRef="Scofield:Re 13:8"µµRevelation 13:8@@/referenceµµ")\it*.
\c 4
\p
\v 1
\v 3 \bd world\bd* kosmos = world-system. @@reference osisRef="1jo 5:4 1jo 5:5 1jo 5:19"µµ1 John 5:4,5,19@@/referenceµµ; @@reference osisRef="joh 7:7"µµJohn 7:7@@/referenceµµ. \it (See Scofield "@@reference osisRef="Scofield:Re 13:8"µµRevelation 13:8@@/referenceµµ")\it*.
\v 9 \bd world\bd*
\p kosmos = mankind. \it (See Scofield "@@reference osisRef="Scofield:Mt 4:8"µµMatthew 4:8@@/referenceµµ")\it*.
\v 10 \bd propitiation\bd*
\p (Greek - \w ἱλασμός|strong="G2434"\w*). \it (See Scofield "@@reference osisRef="Scofield:1Jo 2:2"µµ1 John 2:2@@/referenceµµ")\it*.
\v 12 \bd perfected\bd*
\p \it (See Scofield "@@reference osisRef="Scofield:Mt 5:48"µµMatthew 5:48@@/referenceµµ")\it*.
\v 14 \bd Saviour\bd* \it (See Scofield "@@reference osisRef="Scofield:Ro 1:16"µµRomans 1:16@@/referenceµµ")\it*.
\p \bd world\bd* kosmos = mankind. \it (See Scofield "@@reference osisRef="Scofield:Mt 4:8"µµMatthew 4:8@@/referenceµµ")\it*.
\v 18 \bd perfect\bd*
\p \it (See Scofield "@@reference osisRef="Scofield:Mt 5:48"µµMatthew 5:48@@/referenceµµ")\it*.
\c 5
\p
\v 1
\v 4 \bd world\bd*
\p kosmos = world-system. @@reference osisRef="re 11:15"µµRevelation 11:15@@/referenceµµ; @@reference osisRef="joh 7:7"µµJohn 7:7@@/referenceµµ. \it (See Scofield "@@reference osisRef="Scofield:Re 13:8"µµRevelation 13:8@@/referenceµµ")\it*.
\v 7 \bd v. 7\bd*
\p It is generally agreed that v.7 has no real authority, and has been inserted. @@reference osisRef="1jo 5:7"µµ1 John 5:7@@/referenceµµ.
\v 8 \bd in earth\bd* Omit "in earth."
\p \bd agree\bd* Or, are to one point or purpose.
\v 16 \bd sin\bd*
\p Sin. \it (See Scofield "@@reference osisRef="Scofield:Ro 3:23"µµRomans 3:23@@/referenceµµ")\it*.
