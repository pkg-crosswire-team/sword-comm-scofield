\id 2PE
\toc1
\toc2
\toc3
\c 1
\s Book Introduction - 2 Peter
\p @@reference osisRef="2pe 1"µµRead first chapter of 2 Peter@@/referenceµµ
\p \bd WRITER\bd*: The Apostle Peter (@@reference osisRef="2pe 1:1"µµ2 Peter 1:1@@/referenceµµ)
\p \bd DATE\bd*: Probably A.D. 66
\p \bd THEME\bd*: Second Peter and Second Timothy have much in common. In both, the writers are aware that martyrdom is near (@@reference osisRef="2ti 4:6"µµ2 Timothy 4:6@@/referenceµµ; @@reference osisRef="2pe 1:14"µµ2 Peter 1:14@@/referenceµµ with ; @@reference osisRef="joh 21:18 joh 21:19"µµJohn 21:18,19@@/referenceµµ); both are singularly sustained and joyful; both foresee the apostasy in which the history of the professing church will end. Paul finds that apostasy in its last stage when the so-called laity \it (See Scofield "@@reference osisRef="Scofield:Re 2:6"µµRevelation 2:6@@/referenceµµ")\it* , have become infected (@@reference osisRef="2ti 3:1-5"µµ2 Timothy 3:1-5@@/referenceµµ; @@reference osisRef="2ti 4:3 2ti 4:4"µµ 4:3,4@@/referenceµµ); Peter traces the origin of the apostasy to false teachers (@@reference osisRef="2pe 2:1-3 2pe 2:15-19"µµ2 Peter 2:1-3,15-19@@/referenceµµ). In Peter the false teachers deny redemption truth (@@reference osisRef="2pe 2:1"µµ2 Peter 2:1@@/referenceµµ); we shall find in First John a deeper depth--denial of the truth concerning Christ's person (@@reference osisRef="1jo 4:1-5"µµ1 John 4:1-5@@/referenceµµ). In Jude all phases of the apostasy are seen. But in none of these Epistles is the tone one of dejection or pessimism. God and His promises are still the resource of the believer.
\p The Epistle is in four divisions:
\li The great Christian virtues, 1:1-14
\li The Scriptures exalted, 1:15-21
\li Warnings concerning apostate teachers, 2:1-22
\li The second coming of Christ and the day of Jehovah, 3:1-18
\p
\v 2 \bd grace\bd*
\p Grace (imparted). @@reference osisRef="ro 6:1"µµRomans 6:1@@/referenceµµ; @@reference osisRef="2pe 3:18"µµ2 Peter 3:18@@/referenceµµ
\v 4 \bd world\bd*
\p kosmos = world-system. @@reference osisRef="2pe 2:20"µµ2 Peter 2:20@@/referenceµµ; @@reference osisRef="joh 7:7"µµJohn 7:7@@/referenceµµ \it (See Scofield "@@reference osisRef="Scofield:Re 13:8"µµRevelation 13:8@@/referenceµµ")\it*.
\v 9 \bd sins\bd*
\p Sin. \it (See Scofield "@@reference osisRef="Scofield:Ro 3:23"µµRomans 3:23@@/referenceµµ")\it*.
\v 18 \bd holy mount\bd*
\p Where the reference is to things, the meaning of "holy" or "sanctified" is, simply, set apart for the use of God, or rendered sacred by the divine presence.
\v 19 \bd whereunto\bd*
\p That is, made more sure by fulfilment in part. Fulfilled prophecy is a proof of inspiration because the Scripture predictions of future events were uttered so long before the events transpired that no merely human sagacity or foresight could have anticipated them, and these predictions are so detailed, minute, and specific, as to exclude the possibility that they were mere fortunate guesses. Hundreds of predictions concerning Israel, the land of Canaan, Babylon, Assyria, Egypt, and numerous personages--so ancient, so singular, so seemingly improbable, as well as so detailed and definite that no mortal could have anticipated them--have been fulfilled by the elements, and by men who were ignorant of them, or who utterly disbelieved them, or who struggled with frantic desperation to avoid their fulfilment. It is certain, therefore, that the Scriptures which contain them are inspired. "Prophecy came not in olden time by the will of man; but holy men of God spake as they were moved by the Holy Ghost" @@reference osisRef="2pe 1:21"µµ2 Peter 1:21@@/referenceµµ.
\p \bd a more sure\bd* Or, the word of prophecy made more sure.
\p \bd dark place\bd* Or, squalid place. @@reference osisRef="ps 119:105"µµPsalms 119:105@@/referenceµµ; @@reference osisRef="joh 1:4 joh 1:9"µµJohn 1:4,9@@/referenceµµ.
\v 20 \bd any private\bd*
\p its own interpretation; i.e. not isolated from all that the Word has given elsewhere.
\c 2
\p
\v 1
\v 5 \bd world\bd*
\p kosmos = mankind. \it (See Scofield "@@reference osisRef="Scofield:Mt 4:8"µµMatthew 4:8@@/referenceµµ")\it*.
\v 12 \bd natural brute beasts\bd*
\p natural animals without reason. @@reference osisRef="jude 1:10"µµJude 1:10@@/referenceµµ.
\v 14 \bd sin\bd*
\p Sin. \it (See Scofield "@@reference osisRef="Scofield:Ro 3:23"µµRomans 3:23@@/referenceµµ")\it*.
\v 15 \bd Balaam\bd*
\p Balaam. \it (See Scofield "@@reference osisRef="Scofield:Nu 22:5"µµNumbers 22:5@@/referenceµµ")\it* was the typical hireling prophet, anxious only to make a market of his gift. This is the "way" of Balaam. See the "error" of Balaam, See Scofield "@@reference osisRef="Scofield:jude 1:11"µµJude 1:11@@/referenceµµ" and the "doctrine" of Balaam, See Scofield "@@reference osisRef="Scofield:re 2:14"µµRevelation 2:14@@/referenceµµ".
\v 20 \bd world\bd* kosmos = world-system.
\p @@reference osisRef="1jo 2:15-17"µµ1 John 2:15-17@@/referenceµµ; @@reference osisRef="joh 7:7"µµJohn 7:7@@/referenceµµ \it (See Scofield "@@reference osisRef="Scofield:Re 13:8"µµRevelation 13:8@@/referenceµµ")\it*
\p \bd Saviour\bd* \it (See Scofield "@@reference osisRef="Scofield:Ro 1:16"µµRomans 1:16@@/referenceµµ")\it*.
\c 3
\p
\v 1
\v 2 \bd Saviour\bd*
\p \it (See Scofield "@@reference osisRef="Scofield:Ro 1:16"µµRomans 1:16@@/referenceµµ")\it*.
\v 6 \bd world\bd*
\p kosmos = mankind. \it (See Scofield "@@reference osisRef="Scofield:Mt 4:8"µµMatthew 4:8@@/referenceµµ")\it*.
\v 13 \bd righteousness\bd*
\p \it (See Scofield "@@reference osisRef="Scofield:1Jo 3:7"µµ1 John 3:7@@/referenceµµ")\it*.
\v 15 \bd salvation\bd*
\p \it (See Scofield "@@reference osisRef="Scofield:Ro 1:16"µµRomans 1:16@@/referenceµµ")\it*.
\v 18 \bd grace\bd* Grace (imparted). Summary (see "Grace,") \it (See Scofield "@@reference osisRef="Scofield:Joh 1:17"µµJohn 1:17@@/referenceµµ")\it* grace is not only dispensationally a method of divine dealing in salvation
\p \it (See Scofield "@@reference osisRef="Scofield:Joh 1:17"µµJohn 1:17@@/referenceµµ")\it* but is also the method of God in the believer's life and service. As saved, he is "not under the law, but under grace" @@reference osisRef="ro 6:14"µµRomans 6:14@@/referenceµµ. Having by grace brought the believer into the highest conceivable position. @@reference osisRef="eph 1:6"µµEphesians 1:6@@/referenceµµ. God ceaselessly works through grace, to impart to, and perfect in him, corresponding graces ; @@reference osisRef="joh 15:4 joh 15:5"µµJohn 15:4,5@@/referenceµµ; @@reference osisRef="ga 5:22 ga 5:23"µµGalatians 5:22,23@@/referenceµµ.
\p Grace, therefore, stands connected with service @@reference osisRef="ro 12:6"µµRomans 12:6@@/referenceµµ; @@reference osisRef="ro 15:15 ro 15:16"µµ 15:15,16@@/referenceµµ; @@reference osisRef="1co 1:3-7"µµ1 Corinthians 1:3-7@@/referenceµµ; @@reference osisRef="1co 3:10"µµ 3:10@@/referenceµµ; @@reference osisRef="1co 15:10"µµ 15:10@@/referenceµµ; @@reference osisRef="2co 12:9 2co 12:10"µµ2 Corinthians 12:9,10@@/referenceµµ; @@reference osisRef="ga 2:9"µµGalatians 2:9@@/referenceµµ; @@reference osisRef="eph 3:7 eph 3:8"µµEphesians 3:7,8@@/referenceµµ; @@reference osisRef="eph 4:7"µµEphesians 4:7@@/referenceµµ; @@reference osisRef="php 1:7"µµPhilippians 1:7@@/referenceµµ; @@reference osisRef="2ti 2:1 2ti 2:2"µµ2 Timothy 2:1,2@@/referenceµµ; @@reference osisRef="1pe 4:10"µµ1 Peter 4:10@@/referenceµµ with Christian growth ; @@reference osisRef="2co 1:12"µµ2 Corinthians 1:12@@/referenceµµ; @@reference osisRef="eph 4:29"µµEphesians 4:29@@/referenceµµ; @@reference osisRef="col 3:16"µµColossians 3:16@@/referenceµµ; @@reference osisRef="col 4:6"µµ 4:6@@/referenceµµ; @@reference osisRef="2th 1:12"µµ2 Thessalonians 1:12@@/referenceµµ; @@reference osisRef="heb 4:16"µµHebrews 4:16@@/referenceµµ; @@reference osisRef="heb 12:28 heb 12:29"µµ 12:28,29@@/referenceµµ; @@reference osisRef="heb 13:9"µµ 13:9@@/referenceµµ; @@reference osisRef="jas 4:6"µµJames 4:6@@/referenceµµ; @@reference osisRef="1pe 1:2"µµ1 Peter 1:2@@/referenceµµ; @@reference osisRef="1pe 3:7"µµ 3:7@@/referenceµµ; @@reference osisRef="1pe 5:5 1pe 5:10"µµ 5:5,10@@/referenceµµ; @@reference osisRef="2pe 3:18"µµ2 Peter 3:18@@/referenceµµ; @@reference osisRef="jude 1:4"µµJude 1:4@@/referenceµµ and with giving ; @@reference osisRef="2co 4:15"µµ2 Corinthians 4:15@@/referenceµµ; @@reference osisRef="2co 8:1 2co 8:6 2co 8:7 2co 8:19"µµ 8:1,6,7,19@@/referenceµµ; @@reference osisRef="2co 9:14"µµ 9:14@@/referenceµµ
\p \bd grace\bd* Grace (imparted). @@reference osisRef="ro 6:1"µµRomans 6:1@@/referenceµµ; @@reference osisRef="2pe 3:18"µµ2 Peter 3:18@@/referenceµµ.
