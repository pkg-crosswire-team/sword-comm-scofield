\id LEV
\toc1
\toc2
\toc3
\c 1
\s Book Introduction - Leviticus
\p @@reference osisRef="le 1"µµRead first chapter of Leviticus@@/referenceµµ
\p LEVITICUS stands in the same relation to EXODUS, that the Epistles do to the Gospels. EXODUS is the record of redemption, and lays the foundation of the cleansing, worship, and service of a redeemed people. LEVITICUS gives the detail of the walk, worship, and service of that people. In EXODUS God speaks out of the mount to which approach was forbidden; in LEVITICUS He speaks out of the tabernacle in which He dwells in the midst of His people, to tell them that which befits His holiness in their approach to, and communion with, Himself.
\p The key word of Leviticus is holiness, occurring 87 times. Key verse is @@reference osisRef="le 19:2"µµLeviticus 19:2@@/referenceµµ.
\p LEVITICUS is in nine chief divisions:
\li The Offerings 1-6:7
\li The Law of the Offerings6:8-7:38.
\li Consecration8:1-9:24
\li A Warning Example10:1-20.
\li A Holy God Must Have a Cleansed People 11-15.
\li Atonement 16,17.
\li The Relationships of God's People 18-22.
\li The Feasts of Jehovah, 23.
\li Instructions and Warnings, 24-27.
\p
\v 3 \bd burnt-sacrifice\bd*
\p The burnt-offering
\li (1) typifies Christ offering Himself without spot to God in delight to do His Father's will even in death.
\li (2) it is atoning because the believer has not had this delight in the will of God; and
\li (3) substitutionary (@@reference osisRef="le 1:4"µµLeviticus 1:4@@/referenceµµ) because Christ did it in the sinner's stead. But the thought of penalty is not prominent. ; @@reference osisRef="heb 9:11-14"µµHebrews 9:11-14@@/referenceµµ; @@reference osisRef="heb 10:5-7"µµ 10:5-7@@/referenceµµ; @@reference osisRef="ps 40:6-8"µµPsalms 40:6-8@@/referenceµµ; @@reference osisRef="php 2:8"µµPhilippians 2:8@@/referenceµµ. The emphatic words @@reference osisRef="le 1:3-5"µµLeviticus 1:3-5@@/referenceµµ are "burnt-sacrifice," "voluntary," "it shall be accepted for him," and "atonement." The creatures acceptable for sacrifice are five:
\li (1) The bullock, or ox, typifies Christ as the patient and enduring Servant @@reference osisRef="1co 9:9 1co 9:10"µµ1 Corinthians 9:9,10@@/referenceµµ; @@reference osisRef="heb 12:2 heb 12:3"µµHebrews 12:2,3@@/referenceµµ "obedient unto death" ; @@reference osisRef="isa 52:13-15"µµIsaiah 52:13-15@@/referenceµµ; @@reference osisRef="php 2:5-8"µµPhilippians 2:5-8@@/referenceµµ. His offering in this character is substitutionary, for this we have not been.
\li (2) The sheep, or lamb, typifies Christ in unresisting self-surrender to the death of the cross @@reference osisRef="isa 53:7"µµIsaiah 53:7@@/referenceµµ; @@reference osisRef="ac 8:32-35"µµActs 8:32-35@@/referenceµµ.
\li (3) The goat typifies the sinner @@reference osisRef="mt 25:33"µµMatthew 25:33@@/referenceµµ and, when used sacrificially, Christ, as "numbered with the transgressors" ; @@reference osisRef="isa 53:12"µµIsaiah 53:12@@/referenceµµ; @@reference osisRef="lu 23:33"µµLuke 23:33@@/referenceµµ and "made sin," and "a curse" ; @@reference osisRef="ga 3:13"µµGalatians 3:13@@/referenceµµ; @@reference osisRef="2co 5:21"µµ2 Corinthians 5:21@@/referenceµµ as the sinner's substitute.
\li (4,5) The turtle-dove or pigeon. Naturally a symbol of mourning innocency @@reference osisRef="isa 38:14"µµIsaiah 38:14@@/referenceµµ; @@reference osisRef="isa 59:11"µµ 59:11@@/referenceµµ; @@reference osisRef="mt 23:37"µµMatthew 23:37@@/referenceµµ; @@reference osisRef="heb 7:26"µµHebrews 7:26@@/referenceµµ is associated with poverty in @@reference osisRef="le 5:7"µµLeviticus 5:7@@/referenceµµ and speaks of Him who for our sakes become poor @@reference osisRef="lu 9:58"µµLuke 9:58@@/referenceµµ and whose pathway of poverty which began with laying aside "the form of God," ended in the sacrifice through which we became rich ; @@reference osisRef="2co 8:9"µµ2 Corinthians 8:9@@/referenceµµ; @@reference osisRef="php 2:6-8"µµPhilippians 2:6-8@@/referenceµµ. The sacrifice of the poor Man becomes the poor man's sacrifice. @@reference osisRef="lu 2:24"µµLuke 2:24@@/referenceµµ. These grades of typical sacrifice test the measure of our apprehension of the varied aspects of Christ's one sacrifice on the cross. The mature believer should see Christ crucified in all these aspects.
\p
\v 4 \bd put his hand upon\bd*
\p The laying of the offerer's hand signified acceptance and identification if himself with his offering. In type it answered to the believer's faith accepting and identifying himself with Christ @@reference osisRef="ro 4:5"µµRomans 4:5@@/referenceµµ; @@reference osisRef="ro 6:3-11"µµ 6:3-11@@/referenceµµ. The believer is justified by faith, and his faith is reckoned for righteousness, because his faith identifies him with Christ, who died as his sin-offering ; @@reference osisRef="2co 5:21"µµ2 Corinthians 5:21@@/referenceµµ; @@reference osisRef="1pe 2:24"µµ1 Peter 2:24@@/referenceµµ.
\p \bd atonement\bd* \it (See Scofield "@@reference osisRef="Scofield:Ex 29:33"µµExodus 29:33@@/referenceµµ")\it*.
\v 8 Fire. Essentially as symbol of God's holiness. As such it expresses God in three ways:
\li (1) In judgment upon that which His holiness utterly condemns (e.g.) @@reference osisRef="ge 19:24"µµGenesis 19:24@@/referenceµµ; @@reference osisRef="mr 9:43-48"µµMark 9:43-48@@/referenceµµ; @@reference osisRef="re 20:15"µµRevelation 20:15@@/referenceµµ.
\li (2) in the manifestation of Himself, and of that which He approves @@reference osisRef="ex 3:2"µµExodus 3:2@@/referenceµµ; @@reference osisRef="1pe 1:7"µµ1 Peter 1:7@@/referenceµµ; @@reference osisRef="ex 13:21"µµExodus 13:21@@/referenceµµ
\p and (3) in purification (e.g) @@reference osisRef="1co 3:12-14"µµ1 Corinthians 3:12-14@@/referenceµµ; @@reference osisRef="mal 3:2 mal 3:3"µµMalachi 3:2,3@@/referenceµµ. So, in Leviticus, the fire which only manifests the sweet savour of the burnt-, meal-, and peace- offerings, wholly consumes the sin-offering.
\p \bd fat\bd* That which burns most quickly -- devotedness, zeal.
\v 9 \bd sweet savour\bd*
\p The sweet savour offerings are so called because they typify Christ in His own perfections, and in His affectionate devotion to the Father's will. The non-sweet savour offerings typify Christ as bearing the whole demerit of the sinner. Both are substantial. In our place Christ, in the burnt-offering, makes good our lack of devotedness, and, in the sin- and trespass-offerings, suffers because of our disobediences.
\p
\c 2
\p
\v 1 \bd fine flour\bd*
\p The meal-offering. The fine flour speaks of the evenness and balance of the character of Christ; of that perfection in which no quality was in excess, none lacking; the fire, of His testing by suffering, even unto death; frankincense; the fragrance of His life Godward (see) @@reference osisRef="ex 30:34"µµExodus 30:34@@/referenceµµ absence of leaven, His character as "the Truth" (see) @@reference osisRef="ex 12:8"µµExodus 12:8@@/referenceµµ absence of honey;--His was not that mere natural sweetness which may exist quite apart from grace; oil mingled, Christ as born of the Spirit @@reference osisRef="mt 1:18-23"µµMatthew 1:18-23@@/referenceµµ oil upon, Christ as baptized with the Spirit ; @@reference osisRef="joh 1:32"µµJohn 1:32@@/referenceµµ; @@reference osisRef="joh 6:27"µµ 6:27@@/referenceµµ the oven, the unseen sufferings of Christ--His inner agonies ; @@reference osisRef="heb 2:18"µµHebrews 2:18@@/referenceµµ; @@reference osisRef="mt 27:45 mt 27:46"µµMatthew 27:45,46@@/referenceµµ the pan, His more evident sufferings (e.g.) @@reference osisRef="mt 27:27-31"µµMatthew 27:27-31@@/referenceµµ salt, the pungency of the truth of God--that which arrests the action of leaven.
\v 11 \bd leaven\bd*
\p \bd honey\bd*
\p For meanings of leaven see Mat 13:.33. Also @@reference osisRef="le 7:13"µµLeviticus 7:13,@@/referenceµµ \it (See Scofield "@@reference osisRef="Scofield:Le 7:13"µµLeviticus 7:13@@/referenceµµ")\it*
\p 2 Honey is mere natural sweetness and could not symbolize the divine graciousness of the Lord Jesus.
\v 13 \bd salt\bd*
\p Cf. @@reference osisRef="nu 18:19"µµNumbers 18:19@@/referenceµµ; @@reference osisRef="mr 9:49 mr 9:50"µµMark 9:49,50@@/referenceµµ; @@reference osisRef="col 4:6"µµColossians 4:6@@/referenceµµ
\p
\c 3
\p
\v 1 \bd peace-offering\bd*
\p The peace-offering. The whole work of Christ in relation to the believer's peace is here in type. He made peace, @@reference osisRef="col 1:20"µµColossians 1:20@@/referenceµµ proclaimed peace, @@reference osisRef="eph 2:17"µµEphesians 2:17@@/referenceµµ and is our peace, @@reference osisRef="eph 2:14"µµEphesians 2:14@@/referenceµµ.
\p In Christ God and the sinner meet in peace; God is propitiated, the sinner reconciled-- both alike satisfied with what Christ has done. But all this at the cost of blood and fire. The details speak of fellowship. This brings in prominently the thought of fellowship with God through Christ. Hence the peace-offering is set forth as affording food for the priests @@reference osisRef="le 7:31-34"µµLeviticus 7:31-34@@/referenceµµ. Observe that it is the breast (affections) and shoulders (strength) upon which we as priests @@reference osisRef="1pe 2:9"µµ1 Peter 2:9@@/referenceµµ feed in fellowship with the Father. This it is which makes the peace-offering especially a thank-offering. @@reference osisRef="le 7:11 le 7:12"µµLeviticus 7:11,12@@/referenceµµ.
\v 4 \bd caul\bd* Fat appendage.
\c 4
\p
\v 1
\v 3 \bd sin-offering\bd*
\p The sin-offering, though still Christ, is Christ seen laden with the believer's sin, absolutely in the sinner's place and stead, and not, as in the sweet savour offerings, in His own perfections. It is Christ's death as viewed in @@reference osisRef="isa 53:1-12"µµIsaiah 53:1-12@@/referenceµµ; @@reference osisRef="ps 22:1-31"µµPsalms 22:1-31@@/referenceµµ; @@reference osisRef="mt 26:28"µµMatthew 26:28@@/referenceµµ; @@reference osisRef="1pe 2:24"µµ1 Peter 2:24@@/referenceµµ; @@reference osisRef="1pe 3:18"µµ 3:18@@/referenceµµ.
\p But note @@reference osisRef="le 6:24-30"µµLeviticus 6:24-30@@/referenceµµ how the essential holiness of Him who was "made sin for us" @@reference osisRef="2co 5:21"µµ2 Corinthians 5:21@@/referenceµµ is guarded. The sin-offerings are expiatory, substitutional, efficacious @@reference osisRef="le 4:12 le 4:29 le 4:35"µµLeviticus 4:12,29,35@@/referenceµµ and have in view the vindication of the law through substitutional sacrifice.
\v 12 \bd without the camp\bd*
\p Cf. @@reference osisRef="ex 29:14"µµExodus 29:14@@/referenceµµ; @@reference osisRef="le 16:27"µµLeviticus 16:27@@/referenceµµ; @@reference osisRef="nu 19:3"µµNumbers 19:3@@/referenceµµ; @@reference osisRef="heb 13:10-13"µµHebrews 13:10-13@@/referenceµµ. The last passage is the interpretative one. The "camp" was Judaism-- a religion of forms and ceremonies. "Jesus, also, that He might sanctify separate, or set apart for God] the people with or 'through' His own blood, suffered without the gate" temple gate, city gate, i.e. Judaism civil and religious]; @@reference osisRef="heb 13:12"µµHebrews 13:12@@/referenceµµ but how does this sanctify, or set apart, a people? "Let us go forth therefore unto Him without the camp Judaism then, Judaized Christianity now--anything religious which denies Him as our sin-offering] bearing His reproach" @@reference osisRef="heb 13:13"µµHebrews 13:13@@/referenceµµ. The sin- offering, "burned without the camp," typifies this aspect of the death of Christ. The cross becomes a new altar, in a new place, where, without the smallest merit in themselves, the redeemed gather to offer, as believer-priests, spiritual sacrifices. ; @@reference osisRef="heb 13:15"µµHebrews 13:15@@/referenceµµ; @@reference osisRef="1pe 2:5"µµ1 Peter 2:5@@/referenceµµ. The bodies of the sin-offering beasts were not burned without the camp, as some have fancied, because "saturated with sin," and unfit for a holy camp. Rather, an unholy camp was an unfit place for a holy sin-offering. The dead body of our Lord was not "saturated with sin," though in it our sins had been borne @@reference osisRef="1pe 2:24"µµ1 Peter 2:24@@/referenceµµ.
\v 20 \bd atonement\bd*
\p \it (See Scofield "@@reference osisRef="Scofield:Ex 29:33"µµExodus 29:33@@/referenceµµ")\it*.
\v 26 \bd atonement\bd*
\p \it (See Scofield "@@reference osisRef="Scofield:Ex 29:33"µµExodus 29:33@@/referenceµµ")\it*
\c 5
\p
\v 1
\v 6 \bd trespass offering\bd*
\p The trespass-offerings have in view rather the injury which sin does than its guilt-- which is the sin-offering aspect. What is due to God's rights in every human being is here meant. @@reference osisRef="ps 51:4"µµPsalms 51:4,@@/referenceµµ is a perfect expression of this.
\v 10 \bd atonement\bd*
\p \it (See Scofield "@@reference osisRef="Scofield:Ex 29:33"µµExodus 29:33@@/referenceµµ")\it*.
\c 6
\p
\v 1
\v 13 \bd The Fire\bd*
\p \it (See Scofield "@@reference osisRef="Scofield:Le 1:8"µµLeviticus 1:8@@/referenceµµ")\it*. Here the fire expresses also the undying devotedness of Christ.
\v 30 \bd reconcile\bd*
\p Heb. "kaphar," to cover. \it (See Scofield "@@reference osisRef="Scofield:Da 9:24"µµDaniel 9:24@@/referenceµµ")\it* See Scofield "@@reference osisRef="Scofield:ex 29:33"µµExodus 29:33@@/referenceµµ"
\p
\c 7
\p
\v 1 \bd reconcile\bd*
\p Heb. "kaphar," to cover. \it (See Scofield "@@reference osisRef="Scofield:Da 9:24"µµDaniel 9:24@@/referenceµµ")\it* See Scofield "@@reference osisRef="Scofield:ex 29:33"µµExodus 29:33@@/referenceµµ"
\v 7 \bd atonement\bd*
\p \it (See Scofield "@@reference osisRef="Scofield:Ex 29:33"µµExodus 29:33@@/referenceµµ")\it*
\v 11 \bd peace-offerings\bd*
\p In the "law of the offerings," the peace-offering is taken out of its place as third of the sweet savour offerings, and placed alone, and after all the non-sweet savour offerings. The explanation is as simple as the fact is beautiful. In revealing the offerings Jehovah works from Himself out to the sinner. \it (See Scofield "@@reference osisRef="Scofield:Ex 25:10"µµExodus 25:10@@/referenceµµ")\it*. The whole burnt-offering comes first as meeting what is due to the divine affections, and the trespass-offering last as meeting the simplest aspect of sin-- its injuriousness. But the sinner begins of necessity with that which lies nearest to a newly awakened conscience--a sense, namely, that because of sin he is at enmity with God. His first need, therefore, is peace with God. And that is precisely the Gospel order. Christ's first message is, "Peace" @@reference osisRef="joh 20:19"µµJohn 20:19@@/referenceµµ afterward He shows them His hands and His side. It is the order as @@reference osisRef="2co 5:18-21"µµ2 Corinthians 5:18-21@@/referenceµµ first "the word of reconciliation," @@reference osisRef="le 7:19"µµLeviticus 7:19,@@/referenceµµ then the trespass- and sin-offering, @@reference osisRef="le 7:21"µµLeviticus 7:21@@/referenceµµ. Experience thus reverses the order of revelation.
\v 13 \bd leaven\bd*
\p The use of leaven here is significant. Peace with God is something which the believer shares with God. Christ is our peace-offering @@reference osisRef="eph 2:13"µµEphesians 2:13@@/referenceµµ. Any thanksgiving for peace must, first of all, present Him. In verse 12 we have this, in type, and so leaven is excluded. In verse 13 it is the offerer who gives thanks for his participation in the peace, and so leaven fitly signifies, that though having peace with God through the work of another, there is still evil in him. This is illustrated in @@reference osisRef="am 4:5"µµAmos 4:5@@/referenceµµ where the evil in Israel is before God.
\c 8
\p
\v 1
\v 2 \bd Aaron\bd*
\p The priests did not consecrate themselves, all was done by another, in this instance Moses, acting for Jehovah. The priests simply presented their bodies in the sense of @@reference osisRef="ro 12:1"µµRomans 12:1@@/referenceµµ.
\v 8 \bd Urim and Thummim\bd* \it (See Scofield "@@reference osisRef="Scofield:Ex 28:30"µµExodus 28:30@@/referenceµµ")\it*. Urim, "lights." Thummim, "perfection."
\v 12 \bd poured of the anointing oil\bd*
\p Two important distinctions are made in the case of the high priest, thus confirming his typical relation to Christ the anti-type:
\li (1) Aaron is anointed before the sacrifices are slain, while in the case of the priests the application of blood precedes the anointing. Christ the sinless One required no preparation for receiving the anointing oil, symbol of the Holy Spirit;
\li (2) upon the high priest only was the anointing oil poured. "God giveth not the Spirit by measure unto him" @@reference osisRef="joh 3:34"µµJohn 3:34@@/referenceµµ. "Thy God hath anointed Thee with the oil of gladness above Thy fellows." @@reference osisRef="heb 1:9"µµHebrews 1:9@@/referenceµµ.
\p
\v 15 \bd reconciliation\bd* Heb. "kaphar," to cover.
\p \it (See Scofield "@@reference osisRef="Scofield:Da 9:24"µµDaniel 9:24@@/referenceµµ")\it*.
\v 34 \bd atonement\bd* \it (See Scofield "@@reference osisRef="Scofield:Ex 29:33"µµExodus 29:33@@/referenceµµ")\it*
\p
\c 10
\p
\v 1 Strange fire. Fire "from before the Lord" had kindled upon the altar of burnt-offering the fire which the care of the priests was to keep burning @@reference osisRef="le 6:12"µµLeviticus 6:12@@/referenceµµ. No commandment had yet been given @@reference osisRef="le 16:12"µµLeviticus 16:12@@/referenceµµ how the incense should be kindled. The sin of Nadab and Abihu was in acting in the things of God without seeking the mind of God. It was "will worship" @@reference osisRef="col 2:23"µµColossians 2:23@@/referenceµµ which often has a "show of wisdom and humility." It typifies any use of carnal means to kindle the fire of devotion and praise.
\v 4 Mish-a-el, 'one with God.' El-za-phan, 'God protects.' Uz-zi-el, 'power of God.'
\v 6 El-e-a-zar, 'God has helped.' Ith-a-mar, 'isle of palms.'
\v 17 \bd atonement\bd*
\p \it (See Scofield "@@reference osisRef="Scofield:Ex 29:33"µµExodus 29:33@@/referenceµµ")\it*.
\c 11
\p
\v 1
\v 2 \bd These are the beasts\bd*
\p The dietary regulations of the covenant people must be regarded primarily as sanitary. Israel, it must be remembered, was a nation living on the earth under a theocratic government. Of necessity the divine legislation concerned itself with the social as well as with the religious life of the people. To force upon every word of that legislation a typical meaning is to strain @@reference osisRef="1co 10:1-11"µµ1 Corinthians 10:1-11@@/referenceµµ; @@reference osisRef="heb 9:23 heb 9:24"µµHebrews 9:23,24@@/referenceµµ beyond all reasonable interpretation.
\v 6 \bd hare\bd*
\p Heb. arnebeth, an unidentified animal, but certainly not a hare, possessing as it is said to, characteristics not possessed by the hare. The supposed error in the text is due entirely to the translators' assumption that the English hare and the ancient "arnebeth" were identical.
\c 12
\p
\v 1
\v 7 \bd atonement\bd*
\p \it (See Scofield "@@reference osisRef="Scofield:Ex 29:33"µµExodus 29:33@@/referenceµµ")\it*.
\c 13
\p
\v 1
\v 2 \bd Leprosy\bd*
\p Leprosy speaks of sin as
\li (1) in the blood;
\li (2) becoming overt in loathsome ways;
\li (3) incurable by human means. The anti-type as applied to the people of God is "sin," demanding self-judgment @@reference osisRef="1co 11:31"µµ1 Corinthians 11:31@@/referenceµµ and "sins," demanding confession and cleansing. @@reference osisRef="1jo 1:9"µµ1 John 1:9@@/referenceµµ.
\p
\v 3 \bd priest\bd*
\p Some have found in the regulations of this chapter concerning an inquest by the priest of a case of leprosy, elaborate provisions for the exercise of discipline in the local church. No little self-righteousness and cruelty have come in thereby. The explicit instructions of the N.T. are the alone and sufficient rule of discipline.
\c 14
\p
\v 1
\v 3 \bd go forth\bd*
\p As a type of Gospel salvation the points are:
\li (1) The leper does nothing @@reference osisRef="ro 4:5 ro 4:5"µµRomans 4:5,5@@/referenceµµ.
\li (2) the priest seeks the leper, not the leper the priest @@reference osisRef="lu 19:10"µµLuke 19:10@@/referenceµµ.
\li (3) "with-out shedding of blood is no remission" @@reference osisRef="heb 9:22"µµHebrews 9:22@@/referenceµµ.
\li (4) "and if Christ be not raised, your faith is vain" @@reference osisRef="1co 15:17"µµ1 Corinthians 15:17@@/referenceµµ.
\p
\v 4 \bd birds\bd*
\p The bird slain, and the live bird, dipped in blood and released, present the two aspects of salvation in @@reference osisRef="ro 4:25"µµRomans 4:25@@/referenceµµ "delivered for our offences, and raised again for our justification."
\v 5 \bd vessel\bd*
\p The earthen vessel typifies the humanity of Christ, as the running water typifies the Holy Spirit as the "Spirit of life" @@reference osisRef="ro 8:2"µµRomans 8:2@@/referenceµµ "put to death in the flesh, but quickened by the Spirit." @@reference osisRef="1pe 3:18"µµ1 Peter 3:18@@/referenceµµ
\v 20 Marg
\p \it (See Scofield "@@reference osisRef="Scofield:Ex 29:33"µµExodus 29:33@@/referenceµµ")\it*
\v 29 \bd atonement\bd*
\p \it (See Scofield "@@reference osisRef="Scofield:Ex 29:33"µµExodus 29:33@@/referenceµµ")\it*
\v 53 \bd atonement\bd*
\p \it (See Scofield "@@reference osisRef="Scofield:Ex 29:33"µµExodus 29:33@@/referenceµµ")\it*.
\c 15
\p
\v 1
\v 30 \bd atonement\bd*
\p \it (See Scofield "@@reference osisRef="Scofield:Ex 29:33"µµExodus 29:33@@/referenceµµ")\it*.
\c 16
\p
\v 1
\v 5 \bd goats\bd*
\p The two goats. The offering of the high priest for himself has no anti-type in Christ @@reference osisRef="heb 7:26 heb 7:27"µµHebrews 7:26,27@@/referenceµµ. The typical interest centres upon the two goats and the high priest. Typically
\li (1) all is done by the high priest @@reference osisRef="heb 1:3"µµHebrews 1:3@@/referenceµµ "by Himself"), the people only bring the sacrifice ; @@reference osisRef="mt 26:47"µµMatthew 26:47@@/referenceµµ; @@reference osisRef="mt 27:24 mt 27:25"µµ 27:24,25@@/referenceµµ.
\li (2) The goat slain (Jehovah's lot) is that aspect of Christ's work which vindicates the holiness and righteousness of God as expressed in the law @@reference osisRef="ro 3:24-26"µµRomans 3:24-26@@/referenceµµ and is expiatory.
\li (3) The living goat typifies that aspect of Christ's work which puts away our sins from before God @@reference osisRef="heb 9:26"µµHebrews 9:26@@/referenceµµ; @@reference osisRef="ro 8:33 ro 8:34"µµRomans 8:33,34@@/referenceµµ.
\li (4) The high priest entering the holiest, typifies Christ entering "heaven itself" with "His own blood" for us @@reference osisRef="heb 9:11 heb 9:12"µµHebrews 9:11,12@@/referenceµµ. His blood makes that to be a "throne of grace," and "mercy seat" which else must have been a throne of judgment.
\li (5) For us, the priests of the New Covenant, there is what Israel never had, a rent veil @@reference osisRef="mt 27:51"µµMatthew 27:51@@/referenceµµ; @@reference osisRef="heb 10:19 heb 10:20"µµHebrews 10:19,20@@/referenceµµ. So that, for worship and blessing, we enter, in virtue of His blood, where He is, into the holiest ; @@reference osisRef="heb 4:14-16"µµHebrews 4:14-16@@/referenceµµ; @@reference osisRef="heb 10:19-22"µµ 10:19-22@@/referenceµµ. The atonement of Christ, as interpreted by the O.T. sacrificial types, has these necessary elements:
\li (1) It is substitutionary--the offering takes the offerer's place in death.
\li (2) The law is not evaded but honored--every sacrificial death was an execution of the sentence of the law.
\li (3) The sinlessness of Him who bore our sins is expressed in every animal sacrifice--it must be without blemish.
\li (4) The effect of the atoning work of Christ is typified
\li2 (a) in the promises, "it shall be forgiven him"; and (b) in the peace-offering, the expression of fellowship--the highest privilege of the saint. \it (See Scofield "@@reference osisRef="Scofield:Ex 29:33"µµExodus 29:33@@/referenceµµ")\it*
\p
\v 6 \bd Atonement\bd*
\p Atonement. The biblical use and meaning of the word must be sharply distinguished from its use in theology. In theology it is term which covers the whole sacrificial and redemptive work of Christ. In the O.T. atonement is the English word used to translate the Hebrew words which mean "cover," "coverings," or "to cover." Atonement (at-one-ment) is, therefore, not a translation of the hebrew, but a purely theologic concept. The Levitical offerings "covered" the sins of Israel until, and in anticipation of the Cross, but did not "take away" @@reference osisRef="heb 10:4"µµHebrews 10:4@@/referenceµµ those sins. These were the "sins done aforetime" ("covered" meantime by the Levitical sacrifices), which God "passed over" @@reference osisRef="ro 3:25"µµRomans 3:25@@/referenceµµ for which "passing over" God's righteousness was never vindicated until, in the Cross, Jesus Christ was "set forth a propitiation." See "Propitiation," \it (See Scofield "@@reference osisRef="Scofield:Ro 3:25"µµRomans 3:25@@/referenceµµ")\it*. It was the Cross, not the Levitical sacrifices which made "at-one-ment." The O.T. sacrifices enabled God to go on with a guilty people because they typified the Cross. To the offerer they were the confession of his desert of death, and the expression of his faith; to God they were the "shadows" @@reference osisRef="heb 10:1"µµHebrews 10:1@@/referenceµµ of which Christ was the reality.
\p \bd atonement\bd* \it (See Scofield "@@reference osisRef="Scofield:Ex 29:33"µµExodus 29:33@@/referenceµµ")\it*.
\v 11 \bd atonement\bd* \it (See Scofield "@@reference osisRef="Scofield:Ex 29:33"µµExodus 29:33@@/referenceµµ")\it*.
\v 16 \bd atonement\bd* \it (See Scofield "@@reference osisRef="Scofield:Ex 29:33"µµExodus 29:33@@/referenceµµ")\it*.
\v 18 \bd out unto the altar\bd*
\p Dispensationally, for Israel, this is yet future; the High Priest is still in the holiest. When He comes out to His ancient people they will be converted and restored @@reference osisRef="ro 11:23-27"µµRomans 11:23-27@@/referenceµµ; @@reference osisRef="zec 12:10 zec 12:12"µµZechariah 12:10,12@@/referenceµµ; @@reference osisRef="zec 13:1"µµ 13:1@@/referenceµµ; @@reference osisRef="re 1:7"µµRevelation 1:7@@/referenceµµ Meantime, believers of this dispensation as priests @@reference osisRef="1pe 2:9"µµ1 Peter 2:9@@/referenceµµ enter into the holiest where He is. @@reference osisRef="heb 10:19-22"µµHebrews 10:19-22@@/referenceµµ.
\v 20 \bd reconciling\bd* Heb. kaphar = covering. See @@reference osisRef="da 9:24"µµDaniel 9:24@@/referenceµµ. \it (See Scofield "@@reference osisRef="Scofield:Da 9:24"µµDaniel 9:24@@/referenceµµ")\it*.
\v 27 \bd atonement\bd* \it (See Scofield "@@reference osisRef="Scofield:Ex 29:33"µµExodus 29:33@@/referenceµµ")\it*.
\v 29 \bd seventh month\bd* i.e. October.
\c 17
\p
\v 1
\v 11 \bd altar\bd*
\li (1) The value of the "life" is the measure of the value of the "blood." This gives the blood of Christ its inconceivable value. When it was shed the sinless God-man gave His life. "It is not possible that the blood of bulls and of goats could take away sins" @@reference osisRef="heb 10:4"µµHebrews 10:4@@/referenceµµ.
\li (2) it is not the blood in the veins of the sacrifice, but the blood upon the altar which is efficacious. The Scripture knows nothing of salvation by the imitation or influence of Christ's life, but only by that life yielded up on the cross.
\p \bd blood\bd*
\p The meaning of all sacrifice is here explained. Every offering was an execution of the sentence of the law upon a substitute for the offender, and every such offering pointed forward to that substitutional death of Christ which alone vindicated the righteousness of God in passing over the sins of those who offered the typical sacrifices @@reference osisRef="ro 3:24 ro 3:25"µµRomans 3:24,25@@/referenceµµ; @@reference osisRef="ex 29:36"µµExodus 29:36@@/referenceµµ.
\p \bd atonement\bd*
\p \it (See Scofield "@@reference osisRef="Scofield:Ex 29:33"µµExodus 29:33@@/referenceµµ")\it*.
\c 18
\p
\v 1
\v 21 \bd Molech\bd*
\p Called Moloch. @@reference osisRef="ac 7:43"µµActs 7:43@@/referenceµµ.
\c 19
\p
\v 1
\v 14 \bd fear\bd*
\p \it (See Scofield "@@reference osisRef="Scofield:Ps 19:9"µµPsalms 19:9@@/referenceµµ")\it*.
\v 22 \bd atonement\bd*
\p \it (See Scofield "@@reference osisRef="Scofield:Ex 29:33"µµExodus 29:33@@/referenceµµ")\it*.
\v 32 \bd fear\bd*
\p \it (See Scofield "@@reference osisRef="Scofield:Ps 19:9"µµPsalms 19:9@@/referenceµµ")\it*.
\c 21
\p
\v 1
\v 8 \bd sanctify\bd*
\p Verse 8 illustrates the O.T. holiness or sanctification--a person set apart for the service of God.
\p \bd sanctify\bd*
\p Heb. "qodesh." \it (See Scofield "@@reference osisRef="Scofield:Ge 2:3"µµGenesis 2:3@@/referenceµµ")\it*.
\c 23
\p
\v 1
\v 2 \bd feasts\bd*
\p The feasts of Jehovah. As given to Israel, these were simply seven great religious festivals which were to be observed every year. The first three verses of Lev. 23. do not relate to the feasts but separate the sabbath from the feasts.
\v 5 \bd Passover\bd*
\p The Passover, @@reference osisRef="le 23:4 le 23:5"µµLeviticus 23:4,5@@/referenceµµ. This feast is memorial and brings into view redemption, upon which all blessing rests. Typically, it stands for "Christ our passover, sacrificed for us." @@reference osisRef="1co 5:7"µµ1 Corinthians 5:7@@/referenceµµ.
\p \bd first month\bd* i.e. April.
\v 6 \bd bread\bd*
\p The feast of Unleavened Bread, @@reference osisRef="le 23:6-8"µµLeviticus 23:6-8@@/referenceµµ. This feast speaks of communion with Christ, the unleavened wave-loaf, in the full blessing of His redemption, and of a holy walk. The divine order here is beautiful; first redemption, then a holy walk. ; @@reference osisRef="1co 5:6-8"µµ1 Corinthians 5:6-8@@/referenceµµ; @@reference osisRef="2co 7:1"µµ2 Corinthians 7:1@@/referenceµµ; @@reference osisRef="ga 5:7-9"µµGalatians 5:7-9@@/referenceµµ.
\v 10 \bd first fruits\bd*
\p The feast of Firstfruits, @@reference osisRef="le 23:10-14"µµLeviticus 23:10-14@@/referenceµµ. This feast is typical of resurrection--first of Christ, then of "them that are Christ's at His coming" ; @@reference osisRef="1co 15:23"µµ1 Corinthians 15:23@@/referenceµµ; @@reference osisRef="1th 4:13-18"µµ1 Thessalonians 4:13-18@@/referenceµµ.
\v 16 \bd fifty days\bd*
\p The feast of Pentecost, @@reference osisRef="le 23:15-22"µµLeviticus 23:15-22@@/referenceµµ. The anti-type is the descent of the Holy Spirit to form the church. For this reason leaven is present, because there is evil in the church ; @@reference osisRef="mt 13:33"µµMatthew 13:33@@/referenceµµ; @@reference osisRef="ac 5:1 ac 5:10"µµActs 5:1,10@@/referenceµµ; @@reference osisRef="ac 15:1"µµActs 15:1@@/referenceµµ. Observe, it is now loaves; not a sheaf of separate growths loosely bound together, but a real union of particles making one homogenous body. The descent of the Holy Spirit at Pentecost united the separated disciples into one organism. ; @@reference osisRef="1co 10:16 1co 10:17"µµ1 Corinthians 10:16,17@@/referenceµµ; @@reference osisRef="1co 12:12 1co 12:13 1co 12:20"µµ 12:12,13,20@@/referenceµµ.
\v 17 \bd wave-loaves\bd*
\p The wave-loaves were offered fifty days after the wave-sheaf. This is precisely the period between the resurrection of Christ and the formation of the church at Pentecost by the baptism of the Holy Spirit @@reference osisRef="ac 2:1-4"µµActs 2:1-4@@/referenceµµ; @@reference osisRef="1co 12:12 1co 12:13"µµ1 Corinthians 12:12,13@@/referenceµµ. See "Church" ; @@reference osisRef="mt 16:18"µµMatthew 16:18@@/referenceµµ; @@reference osisRef="heb 12:22 heb 12:23"µµHebrews 12:22,23@@/referenceµµ. With the wave-sheaf no leaven was offered, for there was no evil in Christ; but the wave-loaves, typifying the church, are "baken with leaven," for in the church there is still evil.
\v 24 \bd trumpets\bd*
\p The feast of Trumpets, @@reference osisRef="le 23:23-25"µµLeviticus 23:23-25@@/referenceµµ. This feast is a prophetical type and refers to the future regathering of long-dispersed Israel. A long interval elapses between Pentecost and Trumpets, answering to the long period occupied in the pentecostal work of the Holy Spirit in the present dispensation. Study carefully ; @@reference osisRef="isa 18:3"µµIsaiah 18:3@@/referenceµµ; @@reference osisRef="isa 27:13"µµ 27:13@@/referenceµµ (with contexts); @@reference osisRef="isa 58:1-14"µµIsaiah 58:1-14@@/referenceµµ (entire chapter), and ; @@reference osisRef="joe 2:1-joe 3:21"µµJoel 2:1-3:21@@/referenceµµ; in connection with the "trumpets," and it will be seen that these trumpets, always symbols of testimony, are connected with the regathering and repentance of Israel after the church, or pentecostal period is ended. This feast is immediately followed by the day of atonement.
\p \bd seventh month\bd*
\p i.e. October; also @@reference osisRef="le 23:27 le 23:34 le 23:39 le 23:41"µµLeviticus 23:27,34,39,41@@/referenceµµ.
\v 27 \bd atonement\bd*
\p The day of Atonement, @@reference osisRef="le 23:26-32"µµLeviticus 23:26-32@@/referenceµµ. The day is the same described in Lev. 16., but here the stress is laid upon the sorrow and repentance of Israel. In other words, the prophetical feature is made prominent, and that looks forward to the repentance of Israel after her regathering under the Palestinian Covenant, @@reference osisRef="de 30:1-10"µµDeuteronomy 30:1-10@@/referenceµµ preparatory to the second advent of Messiah and the establishment of the kingdom. See the connection between the "trumpet" in @@reference osisRef="joe 2:1"µµJoel 2:1@@/referenceµµ and the mourning which follows in verses @@reference osisRef="joe 2:11-15"µµJoel 2:11-15@@/referenceµµ.
\p Also @@reference osisRef="zec 12:10-13"µµZechariah 12:10-13@@/referenceµµ in connection with the atonement of @@reference osisRef="zec 13:1"µµZechariah 13:1@@/referenceµµ. Historically the "fountain" of @@reference osisRef="zec 13:1"µµZechariah 13:1@@/referenceµµ was opened at the crucifixion, but rejected by the Jews of that and the succeeding centuries. After the regathering of Israel the fountain will be efficaciously "opened" to Israel.
\p \bd atonement\bd*
\p \it (See Scofield "@@reference osisRef="Scofield:Ex 29:33"µµExodus 29:33@@/referenceµµ")\it*.
\v 42 \bd booths\bd*
\p The feast of Tabernacles, @@reference osisRef="le 23:34-44"µµLeviticus 23:34-44@@/referenceµµ is (like the Lord's Supper for the church) both memorial and prophetic --memorial as to redemption out of Egypt @@reference osisRef="le 23:43"µµLeviticus 23:43@@/referenceµµ prophetic as to the kingdom-rest of Israel after her regathering and restoration, when the feast again becomes memorial, not for Israel alone, but for all nations. @@reference osisRef="zec 14:16-21"µµZechariah 14:16-21@@/referenceµµ.
\c 25
\p
\v 1
\v 9 \bd seventh month\bd*
\p i.e. October.
\p \bd atonement\bd* \it (See Scofield "@@reference osisRef="Scofield:Ex 29:33"µµExodus 29:33@@/referenceµµ")\it*.
\v 17 \bd fear\bd*
\p \it (See Scofield "@@reference osisRef="Scofield:Ps 19:9"µµPsalms 19:9@@/referenceµµ")\it*.
\v 25 \bd redeem\bd*
\p Heb. "goel," Redemp. (Kinsman type). \it (See Scofield "@@reference osisRef="Scofield:Isa 59:20"µµIsaiah 59:20@@/referenceµµ")\it*.
\v 30 \bd redeemed\bd*
\p Heb. "goel," Redemp. (Kinsman type). \it (See Scofield "@@reference osisRef="Scofield:Isa 59:20"µµIsaiah 59:20@@/referenceµµ")\it*.
\v 36 \bd fear\bd*
\p \it (See Scofield "@@reference osisRef="Scofield:Ps 19:9"µµPsalms 19:9@@/referenceµµ")\it*
\v 43 \bd fear\bd*
\p \it (See Scofield "@@reference osisRef="Scofield:Ps 19:9"µµPsalms 19:9@@/referenceµµ")\it*
\v 48 \bd redeemed\bd*
\p Heb. "goel," Redemp. (Kinsman type). \it (See Scofield "@@reference osisRef="Scofield:Isa 59:20"µµIsaiah 59:20@@/referenceµµ")\it*.
\v 49 The Kinsman-Redeemer. The word goel is used to indicate both the redemption-- "to free by paying," and the Redeemer--"the one who pays." The case of Ruth and Boaz @@reference osisRef="ru 2:1"µµRuth 2:1@@/referenceµµ; @@reference osisRef="ru 3:10-18"µµ 3:10-18@@/referenceµµ; @@reference osisRef="ru 4:1-10"µµ 4:1-10@@/referenceµµ perfectly illustrates this beautiful type of Christ. See "Redemption, \it (See Scofield "@@reference osisRef="Scofield:Isa 59:20"µµIsaiah 59:20@@/referenceµµ")\it*.
\p \bd redeem\bd* Heb. "goel," Redemp. (Kinsman type).
\p \it (See Scofield "@@reference osisRef="Scofield:Isa 59:20"µµIsaiah 59:20@@/referenceµµ")\it*.
\v 54 \bd redeemed\bd*
\p Heb. "goel," Redemp. (Kinsman type).
\p \it (See Scofield "@@reference osisRef="Scofield:Isa 59:20"µµIsaiah 59:20@@/referenceµµ")\it*.
\p
\c 26
\p
\v 1 \bd Chapter 26\bd*
\p Chapter 26. should be read in connection with Deut. 28., 29., the Palestinian Covenant.
\c 27
\p
\v 1
\v 13 \bd redeemed\bd*
\p Heb. "goel," Redemp. (Kinsman type). \it (See Scofield "@@reference osisRef="Scofield:Isa 59:20"µµIsaiah 59:20@@/referenceµµ")\it*.
\v 15 \bd redeem\bd*
\p Heb. "goel," Redemp. (Kinsman type). \it (See Scofield "@@reference osisRef="Scofield:Isa 59:20"µµIsaiah 59:20@@/referenceµµ")\it*.
\v 19 \bd redeemed\bd*
\p Heb. "goel," Redemp. (Kinsman type). \it (See Scofield "@@reference osisRef="Scofield:Isa 59:20"µµIsaiah 59:20@@/referenceµµ")\it*.
\v 27 \bd redeem\bd*
\p Heb. "goel," Redemp. (Kinsman type). \it (See Scofield "@@reference osisRef="Scofield:Isa 59:20"µµIsaiah 59:20@@/referenceµµ")\it*.
\v 28 \bd redeemed\bd*
\p Heb. "goel," Redemp. (Kinsman type). \it (See Scofield "@@reference osisRef="Scofield:Isa 59:20"µµIsaiah 59:20@@/referenceµµ")\it*.
\v 30 \bd tithe\bd*
\p \it (See Scofield "@@reference osisRef="Scofield:2Co 8:1"µµ2 Corinthians 8:1@@/referenceµµ")\it*.
\v 31 \bd redeem\bd*
\p Heb. "goel," Redemp. (Kinsman type). \it (See Scofield "@@reference osisRef="Scofield:Isa 59:20"µµIsaiah 59:20@@/referenceµµ")\it*.
