\id HAB
\toc1
\toc2
\toc3
\c 1
\s Book Introduction - Habakkuk
\p @@reference osisRef="hab 1"µµRead first chapter of Habakkuk@@/referenceµµ
\p It seems most probable that Habakkuk prophesied in the latter years of Josiah. Of the prophet himself nothing is known. To him the character of Jehovah was revealed in terms of the highest spirituality. He alone of the prophets was more concerned that the holiness of Jehovah should be vindicated than that Israel should escape chastisement. Written just upon the eve of the captivity, Habakkuk was God's testimony to Himself as against both idolatry and pantheism.
\p The book is in five parts:
\li Habakkuk's perplexity in view of the sins of Israel and the silence of God, 1:1-4. Historically this was the time of Jehovah's forbearance because of Josiah's repentance (2Ki22:18-20).
\li The answer of Jehovah to the prophet's perplexity1:5-11.
\li The prophet, thus answered, utters the testimony to Jehovah, 1:12-17; but he will watch for further answers, 2:1.
\li To the watching prophet comes the response of the "vision," 2:20.
\li All ends in Habakkuk's sublime Psalm of the Kingdom.
\p As a whole the Book of Habakkuk raise and answers the question of God's consistency with Himself in view of permitted evil. The prophet thought that the holiness of God forbade him to go on with evil Israel. The answer of Jehovah announces a Chaldean invasion (@@reference osisRef="hab 1:6"µµHabakkuk 1:6@@/referenceµµ), and a world- wide dispersion @@reference osisRef="hab 1:5"µµHabakkuk 1:5@@/referenceµµ). But Jehovah is not mere wrath; "He delighteth in mercy" (@@reference osisRef="mic 7:18"µµMicah 7:18@@/referenceµµ), and introduces into His answers to the perplexed prophet the great promises, ; @@reference osisRef="mic 1:5"µµMicah 1:5@@/referenceµµ; @@reference osisRef="mic 2:3 mic 2:4 mic 2:14 mic 2:20"µµ 2:3,4,14,20@@/referenceµµ.
\v 1 \bd burden\bd* See note 1, \it (See Scofield "@@reference osisRef="Scofield:Isa 13:1"µµIsaiah 13:1@@/referenceµµ")\it*.
\v 5 \bd for I will work\bd*
\p Verse 5 anticipates the dispersion "among the nations" (cf) @@reference osisRef="de 28:64-67"µµDeuteronomy 28:64-67@@/referenceµµ. While Israel as a nation is thus dispersed, Jehovah will "work a work" which Israel "will not believe." @@reference osisRef="ac 13:37-41"µµActs 13:37-41,@@/referenceµµ interprets this prediction of the redemptive work of Christ. It is significant that Paul quotes this to Jews of the dispersion in the synagogue at Antioch.
\c 2
\p
\v 1
\v 2 \bd run that readeth it\bd*
\p Not, as usually quoted, "that he that runneth may read," but, "that he may run that readeth"; i.e. as a messenger of the "vision." Cf. @@reference osisRef="zec 2:4 zec 2:5"µµZechariah 2:4,5@@/referenceµµ
\v 3 \bd appointed time\bd*
\p To the watching prophet comes the response of the "vision" @@reference osisRef="hab 2:2-20"µµHabakkuk 2:2-20@@/referenceµµ). Three elements are to be distinguished:
\li (1) The moral judgment of Jehovah upon the evils practised by dispersed Israel (Hab 5-13,15-19).
\li (2) The future purpose of God that, practised by dispersed Israel (@@reference osisRef="hab 2:5-13 hab 2:15-19"µµHabakkuk 2:5-13,15-19@@/referenceµµ).
\li (2) The future purpose of God that, "the earth shall be filled with the knowledge of the glory of Jehovah, as the waters cover the sea" (@@reference osisRef="hab 2:14"µµHabakkuk 2:14@@/referenceµµ). That this revelation awaits the return of the Lord in glory is shown
\li (a) by the parallel passage in @@reference osisRef="isa 11:9-12"µµIsaiah 11:9-12@@/referenceµµ and
\li (b) by the quotation of verse 3 in @@reference osisRef="heb 10:37 heb 10:38"µµHebrews 10:37,38@@/referenceµµ where the "it" of the "vision" becomes "he" and refers to the return of the Lord. It is then, after the "vision" is fulfilled, that "the knowledge of the glory," etc, shall fill the earth. But
\li (3) meantime, "the just shall live by his faith." This great evangelic word is applied to Jew and Gentile in @@reference osisRef="ro 1:17"µµRomans 1:17@@/referenceµµ to the Gentiles in @@reference osisRef="ga 3:11-14"µµGalatians 3:11-14@@/referenceµµ and to Hebrews (especially) in @@reference osisRef="heb 10:38"µµHebrews 10:38@@/referenceµµ. This opening of life to faith alone, makes possible not only the salvation of the Gentiles during the dispersion of Israel "among the nations" ; @@reference osisRef="hab 1:5"µµHabakkuk 1:5@@/referenceµµ; @@reference osisRef="ga 3:11-14"µµGalatians 3:11-14@@/referenceµµ but also makes possible a believing remnant in Israel while the nation, as such, is in blindness and unbelief, \it (See Scofield "@@reference osisRef="Scofield:Ro 11:1"µµRomans 11:1@@/referenceµµ")\it* with neither priesthood nor temple, and consequently unable to keep the ordinances of the law. Such is Jehovah! In disciplinary government His ancient Israel is cast out of the land and judicially blinded @@reference osisRef="2co 3:12-15"µµ2 Corinthians 3:12-15@@/referenceµµ but in covenanted mercy the individual Jew may resort to the simple faith of Abraham ; @@reference osisRef="ge 15:6"µµGenesis 15:6@@/referenceµµ; @@reference osisRef="ro 4:1-5"µµRomans 4:1-5@@/referenceµµ and be saved. But this does not set aside the Palestinian See Scofield "@@reference osisRef="Scofield:de 30:3"µµDeuteronomy 30:3@@/referenceµµ" and Davidic See Scofield "@@reference osisRef="Scofield:2sa 7:16"µµ2 Samuel 7:16@@/referenceµµ". Covenants, for "the earth shall be filled," etc. (@@reference osisRef="hab 2:14"µµHabakkuk 2:14@@/referenceµµ),and Jehovah will again be in His temple (@@reference osisRef="hab 2:20"µµHabakkuk 2:20@@/referenceµµ). Cf. ; @@reference osisRef="hab 2:14 hab 2:20"µµHabakkuk 2:14,20@@/referenceµµ; @@reference osisRef="ro 11:25-27"µµRomans 11:25-27@@/referenceµµ
\p
\v 5 \bd hell\bd*
\p Sheol is, in the O.T., the place to which the dead go.
\li (1) Often, therefore, it is spoken of as the equivalent of the grave, merely, where all human activities cease; the terminus toward which all human life moves (e.g. @@reference osisRef="ge 42:38"µµGenesis 42:38@@/referenceµµ grave @@reference osisRef="job 14:13"µµJob 14:13@@/referenceµµ grave @@reference osisRef="ps 88:3"µµPsalms 88:3@@/referenceµµ grave
\li (2) To the man "under the sun," the natural man, who of necessity judges from appearances, sheol seems no more than the grave-- the end and total cessation, not only of the activities of life, but of life itself. @@reference osisRef="ec 9:5 ec 9:10"µµEcclesiastes 9:5,10@@/referenceµµ
\li (3) But Scripture reveals sheol as a place of sorrow @@reference osisRef="2sa 22:6"µµ2 Samuel 22:6@@/referenceµµ; @@reference osisRef="ps 18:5 ps 116:3"µµPsalms 18:5, 116:3@@/referenceµµ; in which the wicked are turned @@reference osisRef="ps 9:17"µµPsalms 9:17@@/referenceµµ and where they are fully conscious ; @@reference osisRef="isa 14:9-17"µµIsaiah 14:9-17@@/referenceµµ; @@reference osisRef="eze 32:21"µµEzekiel 32:21@@/referenceµµ see, especially, @@reference osisRef="jon 2:2"µµJonah 2:2@@/referenceµµ what the belly of the great fish was to Jonah that sheol is to those who are therein). The sheol of the O.T. and hades of the N.T. \it (See Scofield "@@reference osisRef="Scofield:Lu 16:23"µµLuke 16:23@@/referenceµµ")\it* are identical.
\p
\v 13 \bd is it not\bd* Or, it is not of the LORD, etc., i.e. though permitted in His providence, not His plan. Cf. @@reference osisRef="mic 4:2-4"µµMicah 4:2-4@@/referenceµµ.
\v 14 \bd For the earth shall be filled\bd*
\p Cf. @@reference osisRef="isa 11:9"µµIsaiah 11:9@@/referenceµµ which fixes the time when "the earth," etc. It is when David's righteous Branch has set up the kingdom. (See "Kingdom (O.T.)," ; @@reference osisRef="2sa 7:9"µµ2 Samuel 7:9@@/referenceµµ; @@reference osisRef="zec 12:8"µµZechariah 12:8@@/referenceµµ also, "Kingdom (N.T.)," ; @@reference osisRef="lu 1:31-33"µµLuke 1:31-33@@/referenceµµ; @@reference osisRef="1co 15:28"µµ1 Corinthians 15:28@@/referenceµµ. Habakkuk's phrase marks an advance on that of Isaiah. In the latter it is "the knowledge of the Lord." That, in a certain sense, is being diffused now; but in Habakkuk it is "the knowledge of the glory of the Lord," and that cannot be till He is manifested in glory ; @@reference osisRef="mt 24:30"µµMatthew 24:30@@/referenceµµ; @@reference osisRef="mt 25:31"µµ 25:31@@/referenceµµ; @@reference osisRef="lu 9:26"µµLuke 9:26@@/referenceµµ; @@reference osisRef="2th 1:7"µµ2 Thessalonians 1:7@@/referenceµµ; @@reference osisRef="2th 2:8"µµ 2:8@@/referenceµµ; @@reference osisRef="jude 1:14"µµJude 1:14@@/referenceµµ. The transfiguration was a foreview of this. @@reference osisRef="lu 9:26-29"µµLuke 9:26-29@@/referenceµµ.
\v 18 \bd trusteth\bd* \it (See Scofield "@@reference osisRef="Scofield:Ps 2:12"µµPsalms 2:12@@/referenceµµ")\it*
\p
\c 3
\p
\v 1 \bd Prayer\bd*
\p Prayer in the O.T. is in contrast with prayer in the N.T. in two respects:
\li (1) In the former the basis of prayer is a covenant of God, or an appeal to his revealed character as merciful, gracious, etc. In the latter the basis is relationship: "When ye pray, say, Our Father" @@reference osisRef="mt 6:9"µµMatthew 6:9@@/referenceµµ.
\li (2) A comparison, e.g. of the prayers of Moses and Paul, will show that one was praying for an earthly people whose dangers and blessings were earthly; the other for a heavenly people whose dangers and blessings were spiritual.
\p
\v 7 \bd Cushan\bd*
\p Or, Ethiopia.
\v 11 \bd at the light\bd*
\p Or, thine arrows walked in the light, etc.
\v 17 \bd Although\bd*
\p i.e. despite the afflictions of Israel in dispersion, the prophet will rejoice because of the Lord, as yet to return to His temple.
