\id EZR
\toc1
\toc2
\toc3
\c 1
\s Book Introduction - Ezra
\p @@reference osisRef="ezr 1"µµRead first chapter of Ezra@@/referenceµµ
\p Ezra, the first of the post-captivity books (Ezra, Nehemiah, Esther, Haggai, Zechariah, and Malachi), records the return to Palestine under Zerubbabel, by decree of Cyrus, of a Jewish remnant who laid the temple foundations (B.C. 536). Later (B.C. 458) Ezra followed, and restored the law and ritual. But the mass of the nation, and most of the princes, remained by preference in Babylonia and Assyria, where they were prospering. The post-captivity books deal with that feeble remnant which alone had a heart for God.
\p The book is in two parts:
\li From the decree of Cyrus to the dedication of the restored temple, 1:1-6:22.
\li The ministry of Ezra, 7:1-10:44.
\p The events recorded in Ezra cover a period of 80 years (Ussher).
\p
\c 2
\p
\v 1 \bd are the children\bd*
\p Probably individuals from all of the tribes returned to Jerusalem under Zerubbabel, Ezra and Nehemiah, but speaking broadly, the dispersion of the ten tribes (Ephraim- Israel) still continues; nor can they now be positively identified. They are, however, preserved distinct from other peoples and are known to God as such, though they themselves, few in number, know Him not @@reference osisRef="de 28:62"µµDeuteronomy 28:62@@/referenceµµ; @@reference osisRef="isa 11:11-13"µµIsaiah 11:11-13@@/referenceµµ; @@reference osisRef="ho 3:4"µµHosea 3:4@@/referenceµµ; @@reference osisRef="ho 8:8"µµ 8:8@@/referenceµµ.
\p The order of the restoration was as follows:
\li (1) The return of the first detachment under Zerubbabel and Jeshua (B.C. 536), Ezra 1.-6., and the books of Haggai and Zechariah;
\li (2) the expedition of Ezra (B.C. 458), seventy-eight years later (Ezra 7.-10);
\li (3) the commission of Nehemiah (B.C. 444), fourteen years after the expedition of Ezra. @@reference osisRef="ne 2:1-5"µµNehemiah 2:1-5@@/referenceµµ.
\p
\v 2 \bd Zerubbabel\bd*
\p Called Zorobabel, @@reference osisRef="mt 1:12 mt 1:13"µµMatthew 1:12,13@@/referenceµµ.
\v 63 \bd Urim\bd*
\p \it (See Scofield "@@reference osisRef="Scofield:Ex 28:30"µµExodus 28:30@@/referenceµµ")\it*.
\p
\c 3
\p
\v 1 \bd seventh month\bd*
\p i.e. October; also @@reference osisRef="ezr 3:6"µµEzra 3:6@@/referenceµµ.
\v 6 \bd seventh month\bd*
\p i.e. October.
\v 8 \bd second month\bd*
\p i.e. May.
\c 4
\p
\v 2 \bd for we seek\bd*
\p The people of the land sought to hinder the work in three ways:
\li (1) by seeking to draw the Jews into an unreal union, @@reference osisRef="ezr 4:3"µµEzra 4:3@@/referenceµµ. (cf) @@reference osisRef="2ki 17:32"µµ2 Kings 17:32@@/referenceµµ.
\li (2) by "weakening the hands of the people of Judah," @@reference osisRef="ezr 4:4"µµEzra 4:4@@/referenceµµ i.e, by withholding supplies, etc.; and
\li (3) by accusations lodged with Ahasuerus and Darius. The first was by far the most subtle and dangerous. The lives of Ezra and Nehemiah afford many illustrations of true separation. @@reference osisRef="2co 6:14-18"µµ2 Corinthians 6:14-18@@/referenceµµ; @@reference osisRef="2ti 2:19-21"µµ2 Timothy 2:19-21@@/referenceµµ.
\p
\v 6 \bd Ahasuerus\bd*
\p The Cambyses of secular history (529-521 BC); not the Ahasuerus of Esther, who is the Xerxes of secular history (485 BC). \it (See Scofield "@@reference osisRef="Scofield:Da 5:31"µµDaniel 5:31@@/referenceµµ")\it*.
\v 7 \bd Artaxerxes\bd*
\p The Artaxerxes of @@reference osisRef="ezr 4:7"µµEzra 4:7@@/referenceµµ is identical with Ahasuerus of @@reference osisRef="ezr 4:6"µµEzra 4:6,@@/referenceµµ i.e. the Cambyses of profane history. The Artaxerxes of @@reference osisRef="ezr 7:1"µµEzra 7:1@@/referenceµµ is the Longimanus of secular history, BC 418. But \it (See Scofield "@@reference osisRef="Scofield:Da 5:31"µµDaniel 5:31@@/referenceµµ")\it*.
\v 21 \bd Give\bd*
\p Chald. make a new decree.
\c 5
\p
\v 8 \bd great stones\bd*
\p Chald. stones of rolling.
\c 6
\p
\v 6 \bd your companions\bd*
\p Chald. "their societies."
\v 14 \bd finished it\bd*
\p The worship of Jehovah was thus re-established in Jerusalem, but the theocracy was not restored. The remnant which returned from the Babylonian captivity lived in the land by Gentile sufferance, though doubtless by the providential care of Jehovah, till Messiah came, and was crucified by soldiers of the fourth Gentile world-empire Rome, @@reference osisRef="da 2:40"µµDaniel 2:40@@/referenceµµ; @@reference osisRef="da 7:7"µµ 7:7@@/referenceµµ. Soon after (A.D. 70) Rome destroyed the city and temple. See "Times of the Gentiles" ; @@reference osisRef="lu 21:24"µµLuke 21:24@@/referenceµµ; @@reference osisRef="re 16:19"µµRevelation 16:19@@/referenceµµ.
\v 15 \bd Adar\bd*
\p twelfth month i.e. March.
\v 19 \bd first month\bd*
\p i.e. April.
\c 7
\p
\v 8 \bd fifth month\bd*
\p i.e. August.
\v 9 \bd first month\bd* i.e. April.
\p \bd fifth month\bd* i.e. August.
\c 8
\p
\v 31 \bd first month\bd*
\p i.e. April.
\c 10
\p
\v 9 \bd ninth month\bd*
\p i.e. December.
\v 16 \bd tenth month\bd*
\p i.e. January.
\v 17 \bd first month\bd*
\p i.e. April.
\v 40 \bd Machnadebai\bd*
\p Or, Mabnadebai, according to some copies.
